<?php
include 'database.php';

//Check if form submitted
if(isset($_POST['submit'])) {		//cause input for our button has name 'submit',if button is pushed condition is gonna be true 
	//we are gonna grab our POST vars
	$user = mysqli_real_escape_string($con, $_POST['user']);	//we dont want to use it in our query + we gonna wrap it in escape function - that will strip any harmful  submitted(e.g. html tags or )
	$message = mysqli_real_escape_string( $con, $_POST['message']);	//there are a lot of debates about use of escape function(e.g. we may want accept html tags so it depends on context) but its basic exaple where we dont get into high security rules
	
	//Set timezone (user dont write it)
	//if you want more functionality you can add user select their timezone
	if (function_exists('date_default_timezone_set')){
		date_default_timezone_set('Europe/Kiev');
	}
	$time = date('H:i:s');	//standard format


	//Validation
	if(!isset($user) || $user == '' || !isset($message) || $message == ''){
		//if form is incorrect - redirect back to index.php but to show an error_get_last
		$error = "Please fill in your name and a message";
		header("Location: index.php?error=".urlencode($error)); //but + an error with this page so we use our URL which has to be friendly
		exit();
	} else {
		
		$query = "INSERT INTO shouts (user, message, date) VALUES ('$user', '$message', '$time')";	//INSERT query - fields we wonna submit to, VALUES - what we what to insert into this fields (!should match order of this fields)
		//to make sure it actually worked
		if(!mysqli_query($con, $query)){		//if it didnt insert what we want to happen
			die('Error: '.mysqli_error($con));
		}else{			//if it pass we wonna redirect it back to our index.php
			header("Location: index.php");
			exit();
		}
	
	}
	
}
mysqli_close($con);
?>