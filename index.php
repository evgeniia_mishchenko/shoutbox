<?php include 'database.php'; ?>

<?php 
	//Create Select Query
	$query = 'SELECT * FROM shouts ORDER BY id DESC'; 	//the latest posts will appear first 
	$shouts = mysqli_query($con ,$query); //var to hold the results of query
	//at this point nothing gonna happen
	//we need to output it somewhere(in this example it <li> tags using loop)
	
	
	?>
<!DOCTYPE>
<html>
	<head>
		<meta charset="utf-8" />
		<title>SHOUT IT!</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" />
	</head>
	<body>
		<div id="container">
			<header>
				<h1>Shout it! Shoutbox</h1>
			</header>
			<div id="shouts">
				<ul>
					<?php while($row = mysqli_fetch_assoc($shouts)) : ?>		<!-- = while there still rows in our DB that match our query we gonna do sth -->
					<li class="shout"><span><?php echo $row['date']; ?></span> <strong><?php echo $row['user']; ?></strong> : <?php echo $row['message']; ?>  </li>
					<?php endwhile; ?>
					
				</ul>
				
			</div>
			<div id="input">
			<?php if(isset($_GET['error'])) : ?>
				<div id="error"><?php echo $_GET['error']; ?></div>
			<?php endif; ?>
				<form method="POST" action="process.php">
					<input type="text" name="user" placeholder="Enter your name" />
					<input type="text" name="message" placeholder="Enter your message" />
					<br />
					<input class="shout-btn" type="submit" name="submit" value="Send a message" />
				</form>
			</div>
		</div>
	<!-- 
	WHAT TO ADD:
	- registration
	- log in
	- validation
	- add some JS and AJAX (fade in messages)
	
	-->
	
	</body>
</html>